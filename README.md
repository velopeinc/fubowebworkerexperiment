Read Me

## Summary
1. A loading screen is displayed while the data is retrieved
2. The user can navigate the list using the left and right keys
3. Selecting the enter key will open up a overlay information dialog

## Install
```
npm install
```
## Run locally
```
npm run dev
```

## Build for prod
```
npm run build
```

