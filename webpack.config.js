const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
require("@babel/register");

const config = {
    optimization: {
        minimize: false
    },
    output: {
        publicPath: "http://10.0.0.78:443/public/",
        path: __dirname + '/public',
    filename: 'bundle.js'
  },
    plugins: [
        new HtmlWebpackPlugin({
            title: 'Test App',
        })
    ],
  module: {
    rules : [
      {
        test: /\.worker\.js$/,
        use: {
            loader: "worker-loader",
            options: {
                publicPath: '/public/'
            }
        }
      },
      {
        test: /\.js$/,
        exclude: /node_modules/,
        use: ['babel-loader']
      },
      {
        test: /\.css$/,
        use: ['style-loader', 'css-loader']
      },
        {
            test: /\.(gif|png|jpe?g|svg)$/i,
            use: [
                'file-loader',
                {
                    loader: 'image-webpack-loader',
                    options: {
                        bypassOnDebug: true, // webpack@1.x
                        disable: true, // webpack@2.x and newer
                    },
                },
            ],
        }
    ]
  },
  plugins: [
    new HtmlWebpackPlugin({
        hash: true
    })
  ],
  resolve: {
    modules: [
      path.resolve('./src'),
      path.resolve('./node_modules')
    ]
  },
  devServer: {
    contentBase: __dirname + '/public',
    compress: true,
    port: 9000,
    open: true,
    stats: {
        assets: false,
        children: false,
        chunks: false,
        chunkModules: false,
        colors: true,
        entrypoints: false,
        hash: false,
        modules: false,
        timings: false,
        version: false,
    }
  },
  watch: false,
  devtool: 'source-map',
};

module.exports = config;
