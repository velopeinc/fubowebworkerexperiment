// @flow

export type Game = {
    homeTeam: string,
    awayTeam: string,
    gameDate: string,
    homeScore: number,
    awayScore: number,
    thumbnail: gameImage,
    venue: string,
    blurb: string,
}

export type gameImage = {
    aspectRation: string,
    width: number,
    height: number,
    src: string,
    at2x: string,
    at3x: string,
}

export interface ItemRenderer {
    render:Function,
    onFocus: Function,
    onBlur: Function
}