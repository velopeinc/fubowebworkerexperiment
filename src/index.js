import TestApp from 'TestApp'
import PerformanceTable from './utils/PerformanceTable'

const setNoCache = () => {
    const meta3 = document.createElement('meta');
    meta3.httpEquiv = 'cache-control'
    meta3.content = 'no-cache'

    const meta1 = document.createElement('meta');
    meta1.httpEquiv = 'expires'
    meta1.content = '0'

    const meta2 = document.createElement('meta');
    meta2.httpEquiv = 'pragma'
    meta2.content = 'no-cache'

    document.head.appendChild(meta1)
    document.head.appendChild(meta2)
    document.head.appendChild(meta3)
}

const main = () => {
    setNoCache()

    const app = new TestApp()
    window.testApp = app
    app.load()

    window.PerformanceTable = PerformanceTable
}


document.addEventListener('DOMContentLoaded', () => {
    console.log("LOADING MAIN")
    main()
})