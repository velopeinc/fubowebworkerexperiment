// @flow

import LoadingPage from './pages/LoadingPage'
import WebWorkerPage from './pages/WebWorkerPage'
import ImagePageWithPromises from './pages/ImagePageWithPromises'
import BrowserImagePage from './pages/BrowserImagePage'
import ErrorPage from './pages/ErrorPage'
import Worker from "./services/Image.worker"
import AppEventBus from './services/AppEventBus'
import { bogItDown, hangItUp } from "./utils/bogItDown";

const pages = {
    loading: () => new LoadingPage(),
    webWorkers: (query) => new WebWorkerPage(query),
    promises: (query) => new ImagePageWithPromises(query),
    browser: (query) => new BrowserImagePage(query),
    errorPage: () => new ErrorPage()
}

class TestApp {
    worker: Worker;
    currentPage: any = ''
    appContainer: HTMLElement | null;

    constructor() {
        console.log("starting app")
        this.currentPage = pages.loading()
        AppEventBus.subscribe('changePage', this, this.changePage)
        AppEventBus.subscribe('rerender', this, this.render)
    }

    load = () => {
        this.createHostContainer()
        this.render()
    }

    createHostContainer() {
        const appHost = document.createElement('div')
        appHost.id = 'testApp'
        document.body.appendChild(appHost)

        this.appContainer = document.getElementById('testApp')
    }

    changePage({ page, query, getBogged = false, hangItUp = false }: any){
        console.log("REMOVE ", this.currentPage.id)
        window.getBogged = getBogged;
        window.hangItUp = hangItUp
        const toDelete = document.getElementById(this.currentPage.id)
        if(toDelete) this.appContainer.removeChild(toDelete)
        this.currentPage = pages[page](query)
        this.render()
    }

    render = () => {
        this.appContainer.appendChild(this.currentPage.render())
        if(this.currentPage.onFocus) this.currentPage.onFocus()  //if the page is focusable, call focus method.
    }
}

export default TestApp;