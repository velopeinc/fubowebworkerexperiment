// @flow

import DOMUtils from '../utils/DOMUtils'

//TODO make a pool of item renderers instead of making one per data item
//TODO dont let it scroll to empty space at the end
export default class List {

    dataItems:Array<any> = []
    currentIndex = 0
    listElement: HTMLElement
    container: HTMLElement
    id: string
    itemRenderer: any
    itemRenderers: any
    totalWidth: number
    offset: number
    onSelectItem: Function
    accumulatedWidth: number

    //TODO fix itemRenderer type
    constructor(dataItems: Array<any>, onSelectItem: Function, itemRenderer:any, width: number, id: string, ) {
        this.dataItems = dataItems
        this.id = id || "list"
        this.totalWidth = width
        this.accumulatedWidth = 0
        this.itemRenderer = itemRenderer
        this.onSelectItem = onSelectItem
        this.itemRenderers = []
        this.registerKeyHandlers()
    }

    // when not focused, dont respond to keys
    registerKeyHandlers = () => {
        document.addEventListener("keyup", this.handleKeys)
    }

    removeKeyHandlers = () =>{
        document.removeEventListener('keyup', this.handleKeys)
    }

    handleKeys = (code) => {
        if(code.key === "ArrowRight"){
            this.handleNext()
        } else if (code.key === "ArrowLeft") {
            this.handlePrevious()
        } else if (code.key === "Enter"){
            if(this.onSelectItem) this.onSelectItem(this.dataItems[this.currentIndex])        //callback with selected item data
        }

        return false
    }

    //remove focus from the whole list
    removeFocus(){
        const focusedItem = document.getElementById(this.id + this.currentIndex)
        if(focusedItem) focusedItem.blur()
        this.removeKeyHandlers()
    }

    //set focus for the whole list
    setFocus() {
        const focusedItem = document.getElementById(this.id + this.currentIndex)

        this.registerKeyHandlers()
        if(focusedItem) focusedItem.focus()
    }

    //focusing individual items
    onFocus(){
        const focusedItem = document.getElementById(this.id + this.currentIndex)

        if(focusedItem){
            focusedItem.classList.remove('blurred')
            focusedItem.classList.add('selected')
            focusedItem.focus()
        }
        this.itemRenderers[this.currentIndex].onFocus()
    }

    onBlur(){
        const focusedItem = document.getElementById(this.id + this.currentIndex)
        if(focusedItem){
            focusedItem.classList.remove("selected")
            focusedItem.classList.add("blurred")
            focusedItem.blur()
        }
        this.itemRenderers[this.currentIndex].onBlur()
    }

    handleNext= () =>{
        if(this.currentIndex < this.dataItems.length-1){
            this.onBlur()
            this.currentIndex++
            this.onFocus()
            this.scrollListLeft()
        }
    }

    scrollListLeft() {
        const focusedItem = document.getElementById(this.id + this.currentIndex)
        const itemWidth = focusedItem ? DOMUtils.getWidthFromElement(focusedItem) : 0

        this.accumulatedWidth =(this.currentIndex + 1) * itemWidth
        if((this.currentIndex + 1) * itemWidth >= this.totalWidth) {
            const maxItems = Math.ceil(this.totalWidth/itemWidth)
            this.offset = this.accumulatedWidth > this.totalWidth ? this.totalWidth - this.accumulatedWidth : -itemWidth * ((this.currentIndex +1) - maxItems)
            this.listElement.style.transform = "translateX(" + this.offset + "px)"
            this.listElement.style.transition="all 2s ease-in-out;"
        }
    }

    handlePrevious = () =>{
        if( this.currentIndex <= 0) return

        this.onBlur()
        this.currentIndex--
        this.onFocus()
        this.scrollListRight()
    }

    scrollListRight() {
        const focusedItem = document.getElementById(this.id + this.currentIndex) || null;
        const itemWidth = focusedItem ? DOMUtils.getWidthFromElement(focusedItem) : 0
        this.accumulatedWidth -= itemWidth

        if((this.currentIndex) * itemWidth < this.offset * -1) {
            this.offset = this.accumulatedWidth - itemWidth === 0 ? 0 : this.offset - itemWidth * -1
            this.listElement.style.transform = "translateX(" + this.offset + "px)"
            this.listElement.style.transition="all 2s ease-in-out;"
        }
    }

    //render the initial list, todo get width from css, maybe keep key list styling elements here to prevent breakage ie inline-block, overflow hidden
    render(){
        const container = this.container = document.createElement('div')
        container.id='container'
        container.style.width = this.totalWidth + 'px'

        this.listElement = document.createElement("div")
        this.listElement.style.whiteSpace="nowrap"
        this.listElement.id = this.id

        container.appendChild(this.listElement)
        this.dataItems.forEach((item, index) => {
            const itemRenderer = new this.itemRenderer(item, index, this.id)
            this.itemRenderers.push(itemRenderer)
            this.listElement.appendChild(itemRenderer.render())
        })

        return container
    }
}
