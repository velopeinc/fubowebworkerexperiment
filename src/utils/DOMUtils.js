// @flow

export default class DOMUtils{

    static getWidthFromElement = (element: HTMLElement) => {
        const style = getComputedStyle(element)

        const leftMargin = parseInt(style.marginLeft, 10)
        const rightMargin = parseInt(style.marginRight, 10)
        const width = parseInt(style.width, 10)

        return width + leftMargin + rightMargin         //TODO add padding and/or change box-sizing to border-box
    }

    static createTextNode = (label: string, id: string) => {
        const div = document.createElement('div')
        div.id = id

        const text = document.createTextNode(label)
        div.appendChild(text)

        return div
    }

    static createImageNode = (url: string, width: number, height: number, id: string) => {

        const img = document.createElement('img')
        img.id = id
        img.src = url
        img.width = width
        img.height = height

        return img
    }

    //not really necessary - just use createTextNode
    static createParagraphNode = (text: string, id: string) => {
        const div = document.createElement('p')
        div.id = id

        const textNode = document.createTextNode(text)
        div.appendChild(textNode)

        return div
    }

    static createButtonNode = (title: string, onclick: function, id: string) => {
        const div = document.createElement('button')
        div.id = id

        const button = document.createElement('button')
        button.innerHTML = title
        button.onclick = onclick

        div.appendChild(button)

        return div
    }

}

