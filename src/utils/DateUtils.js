// ISO String 2011-10-05T14:48:00.000Z
// @flow

export const getDashFormattedDate = (date: Date) => {
    const dateString = new Date(date.getTime() - (date.getTimezoneOffset() * 60000 ))
        .toISOString()
        .split("T")[0]

    return dateString
}