
const metrics = []

export default class PerformanceTable {
    static addMetric(metric: string[]){
        metrics.push(metric)
    }

    static printTable(){
        console.table(metrics)
    }
}