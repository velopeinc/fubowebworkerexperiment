
// @flow
import '../main.css'

export default class ErrorPage {

    id = 'errorPage'

    render(){
        const page = document.createElement("div")
        page.id=this.id

        const title:HTMLHeadingElement = document.createElement('h1')
        if(title && title.textContent){
            title.textContent = "Error Page"
        } 
        page.appendChild(title)

        return page
    }
}