
// @flow

import { ItemRenderer} from "../../types";
import DOMUtils from "../../utils/DOMUtils";

export default class BrowserImageRenderer implements ItemRenderer{
    imageUrl: string;
    index = 0;
    id = ''

    constructor(imageUrl:string, index: number, id: string){
        this.imageUrl = imageUrl;
        this.index = index;
        this.id = id
    }

    onFocus(){

    }

    onBlur(){

    }

    render(){
        const imageListDetail = document.createElement("div")
        imageListDetail.id = this.id + this.index
        imageListDetail.classList.add('imageListItem')

        const container = document.createElement('div')
        container.id = "contentContainer"+this.index

        const image = document.createElement('img')
        image.src = this.imageUrl
        container.appendChild(image)
        
        imageListDetail.appendChild(container)

        return imageListDetail
    }
}
