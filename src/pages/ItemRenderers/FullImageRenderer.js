
// @flow

import { ItemRenderer} from "../../types";
import DOMUtils from "../../utils/DOMUtils";

export default class ImageItemRenderer implements ItemRenderer{
    image: any;
    index = 0;
    id = ''

    constructor(image:any, index: number, id: string){
        this.image = image;
        this.index = index;
        this.id = id
    }

    onFocus(){

    }

    onBlur(){

    }

    render(){
        const imageListDetail = document.createElement("div")
        imageListDetail.id = this.id + this.index
        imageListDetail.classList.add('imageListItem')

        const container = document.createElement('div')
        container.id = "contentContainer"+this.index

        container.appendChild(this.image)
        imageListDetail.appendChild(container)

        return imageListDetail
    }
}
