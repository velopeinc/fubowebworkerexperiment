// @flow

import { ItemRenderer} from "../../types";
import DOMUtils from "../../utils/DOMUtils";
import type { Game } from '../../types'

export default class GameDetailItemRenderer implements ItemRenderer{

    handleClick: Function
    game: Game

    constructor(game: Game, handleOKClick: Function){
        this.game = game
        this.handleClick = handleOKClick
        document.addEventListener('keyup', this.handleKeys)
    }

    handleKeys = (code) => {
        if(code.key === 'Enter'){
            document.removeEventListener('keyup', this.handleKeys)
            this.handleClick()
        }
    }

    onFocus = () =>{
        const button = document.getElementById("detailOKButton")
        button.focus()
    }

    onBlur = () => {

    }

    render(){
        const gameDetail = document.createElement('div')
        gameDetail.id = "gameDetail"

        const container = document.createElement('div')
        container.id = "gameDetailContainer"

        const title = this.game.homeTeam + " vs " + this.game.awayTeam
        const homeTeam = DOMUtils.createTextNode(title, "gameDetailTitle")
        container.appendChild(homeTeam)

        const blurb = DOMUtils.createParagraphNode(this.game.blurb, "gameBlurb")
        container.appendChild(blurb)

        const okButton = document.createElement('div')
        okButton.id = "detailOKButton"
        okButton.innerText = 'OK'

        container.appendChild(okButton)
        gameDetail.appendChild(container)

        return gameDetail
    }
}