// @flow

import type {  Game } from '../../types'
import DOMUtils from '../../utils/DOMUtils'
import { ItemRenderer } from "../../types";

export default class GameItemRenderer implements ItemRenderer {

    game: Game
    index: number
    id: string

    constructor(game: Game, index: number, id: string){
        this.game = game
        this.index = index  //my position in the list
        this.id = id    //css name
    }

    onFocus = () => {
        this.createFocusedElement()     //when the item receives focus from the list, render the metadata
    }

    onBlur = () =>{  //when defocused, remove the metadata, id and index naming helps to easily identify
        const myElement = document.getElementById("contentContainer"+this.index)
        const footer = document.getElementById('gameFooterDiv')
        const header = document.getElementById('gameHeaderDiv')

        if(footer) myElement.removeChild(footer)
        if(header) myElement.removeChild(header)
    }

    createFocusedElement = () => {
        const myElement = document.getElementById('contentContainer' + this.index)
        const footerDiv = document.createElement('div')
        footerDiv.id = 'gameFooterDiv'

        const homeTeam = DOMUtils.createTextNode(this.game.homeTeam, "homeTeam")
        const awayTeam = DOMUtils.createTextNode(this.game.awayTeam, "awayTeam")
        footerDiv.appendChild(homeTeam)
        footerDiv.appendChild(awayTeam)

        const headerDiv = document.createElement('div')
        headerDiv.id = 'gameHeaderDiv'

        const venue = DOMUtils.createTextNode(this.game.venue, "header")
        headerDiv.appendChild(venue)

        myElement.prepend(headerDiv)
        myElement.appendChild(footerDiv)
    }

    render() {
        const gameListDetail = document.createElement("div")
        gameListDetail.id = this.id + this.index
        gameListDetail.classList.add("gameListItem")

        const container = document.createElement('div')
        container.id = "contentContainer"+this.index

        const thumbnail = this.game.thumbnail
        const img = DOMUtils.createImageNode(thumbnail.src, thumbnail.width, thumbnail.height, "thumbnail"+this.index)
        container.appendChild(img)
        gameListDetail.appendChild(container)

        return gameListDetail
    }
}
