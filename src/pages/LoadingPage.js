// @flow
import DOMUtils from "../utils/DOMUtils"
import '../main.css'

export default class LoadingPage {

    id = 'loadingPage'

    render(){
        const page = document.createElement("div")
        page.id=this.id

        const instructions = DOMUtils.createTextNode("Type window.testApp.changePage({page: 'webWorkers'} to load images with WebWorkers")
        page.appendChild(instructions)

        const instructions2 = DOMUtils.createTextNode("Type window.testApp.changePage({page: 'promises'} to load images with Promises")
        page.appendChild(instructions2)

        const instructions3 = DOMUtils.createTextNode("Type window.testApp.changePage({page: 'browser'} to let the browser load the images")
        page.appendChild(instructions3)

        const instructions4 = DOMUtils.createTextNode("Type window.testApp.changePage({page: 'loading'} to return to start")
        page.appendChild(instructions4)

        const instructions5 = DOMUtils.createTextNode("To change the query term, add a query param to changePage - ({page: 'loading', query:'cats'})" )
        const instructions6 = DOMUtils.createTextNode("To bog UI down, add getBogged:true to params")
        const instructions7 = DOMUtils.createTextNode("To block UI thread completely, add hangItUp:true to params")
        page.appendChild(instructions5)
        page.appendChild(instructions6)
        page.appendChild(instructions7)
        return page
    }

    _focusButton(name){
        const focused = document.getElementById(name)
            this.focusedButton = name
            focused.focus()
    }
}
