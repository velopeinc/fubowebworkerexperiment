// @flow

import DataStore from "../model/DataStore";
import List from '../components/List'
import ImageRenderer from './ItemRenderers/ImageRenderer'
import DOMUtils from "../utils/DOMUtils";
import DataService from '../services/DataService';
import Worker from "services/Image.worker"
import AppEventBus from 'services/AppEventBus'
import PerformanceTable from '../utils/PerformanceTable'
import '../main.css'
import {hangItUpOrBogItDown} from "../utils/bogItDown";

//TODO add an IFocusable interface

export default class WebWorkerPage {
    id = 'webWorkerPage'
    imageUrls = [];
    list: any;
    worker: Worker;
    page
    t1:number
    t2:number

    constructor(query) {
        //listen to the data store then store copy
        console.log("QUERY", query)
       this.t1 = performance.now()
        hangItUpOrBogItDown()
        DataService.getImageUrls(query)
            .then((urls) => {
                this.worker.postMessage(urls)
            })

        this.worker = new Worker();
        this.worker.addEventListener("message", this.onImagesLoaded)

        document.addEventListener("keyup", this.handleKeys)

        DataStore.addDataListener("images", (data) => {
            this.imageUrls = data
            AppEventBus.dispatch('rerender')
        })

        this.page = document.createElement("div")
        this.page.id=this.id

        const title = DOMUtils.createTextNode('WebWorker', 'imagePageTitle');
        this.page.appendChild(title)
    }

    handleKeys(event) {
        if(event.code === 'Backspace'){
            document.removeEventListener('keyup', this.handleKeys)
            AppEventBus.dispatch('changePage', {page: 'loadingPage'})
        }
    }

     onImagesLoaded = (workerData: any) => {
        DataStore.storeData('images', workerData.data);
    }

    //TODO come up with an interpage focus handler onLeft, onRight, onUp, onDown, onBack
    onFocus() {
        if(this.list) this.list.onFocus()
    }

    onClose(appContainer){
        const div = document.getElementById(this.id)
        appContainer.removeChild(div)
    }

    render(){
        console.log("RENDER WebWorkerPage")

        if(this.imageUrls.length > 0) {
            this.list = new List(this.imageUrls, () => {}, ImageRenderer, 1800, 'imagesList' )       //TODO dont pass size here, get it from the CSS
            this.list.setFocus()
            this.page.appendChild(this.list.render())
            this.t2 = performance.now()
            PerformanceTable.addMetric(['webWorker', this.t2 - this.t1])
        }

        return this.page
    }
}