// @flow

import DataStore from "../model/DataStore";
import type { Game } from '../types'
import List from '../components/List'
import GameItemRenderer from './ItemRenderers/GameItemRenderer'
import GameDetailItemRenderer from './ItemRenderers/GameDetailItemRenderer'
import '../main.css'

//TODO add an IFocusable interface

export default class GamesPage {

    id = 'gamePage'
    games: Array<Game> = []
    list: List

    constructor(){
        //listen to the data store then store copy
        DataStore.addDataListener("games", (data) => {
            this.games = data
        })
    }

    //TODO come up with an interpage focus handler onLeft, onRight, onUp, onDown, onBack
    onFocus() {
        this.list.onFocus()
    }

    handleSelectGame = (game: Game) => {
        this.list.removeFocus()
        const myElement = document.getElementById('gamePage')
        const gameDetail = new GameDetailItemRenderer(game, this.handleCloseDetailPage)
        const gameNode = gameDetail.render()        //get the HTML for the detail page
        myElement.appendChild(gameNode)

        gameDetail.onFocus()
    }

    handleCloseDetailPage = () => {
        const myElement = document.getElementById('gamePage')
        const detail = document.getElementById('gameDetail')
        if(detail) myElement.removeChild(detail)
        this.list.setFocus()
        return false
    }

    render(){
        const page = document.createElement("div")
        page.id=this.id

        this.list = new List(this.games, this.handleSelectGame, GameItemRenderer, 1800, 'gamesList' )       //TODO dont pass size here, get it from the CSS
        this.list.setFocus()
        page.appendChild(this.list.render())

        return page
    }
}