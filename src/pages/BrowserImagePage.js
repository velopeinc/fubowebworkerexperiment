// @flow
import DataStore from "../model/DataStore";
import List from '../components/List'
import BrowserImageRenderer from './ItemRenderers/BrowserImageRenderer'
import DOMUtils from "../utils/DOMUtils";
import DataService from '../services/DataService';
import AppEventBus from '../services/AppEventBus'
import PerformanceTable from '../utils/PerformanceTable'
import '../main.css'
import {hangItUpOrBogItDown} from "../utils/bogItDown";

export default class BrowserImagePage {
    id = 'browserPage'
    imageUrls = [];
    list: any;
    page
    t1: number
    t2: number

    constructor(query) {
        //listen to the data store then store copy
        this.t1=performance.now();
        document.addEventListener("keyup", this.handleKeys)

        this.page = document.createElement("div")
        this.page.id=this.id

        const title = DOMUtils.createTextNode('BrowserImagePage', 'imagePageTitle');
        this.page.appendChild(title)

        hangItUpOrBogItDown()
        DataStore.addDataListener("images", (data) => {
            this.imageUrls = data
            console.log("RERENDER")
            AppEventBus.dispatch('rerender')
        })

        DataService.getImageUrls(query)
            .then((urlArray) => {
               DataStore.storeData('images', urlArray)
            })
    }


    handleKeys(event) {
        if(event.code === 'Backspace'){
            document.removeEventListener('keyup', this.handleKeys)
            AppEventBus.dispatch('changePage', {page: 'loadingPage'})
        }
    }

    onFocus() {
        if(this.imageUrls.length > 0) this.list.onFocus()
    }

    render(){
        if(this.imageUrls.length > 0) {
            console.log("RENDER BROWSER PAGE")
            this.list = new List(this.imageUrls, () => {}, BrowserImageRenderer, 1800, 'imagesList' )       //TODO dont pass size here, get it from the CSS
            this.list.setFocus()
            this.page.appendChild(this.list.render())
            this.t2 = performance.now()
            PerformanceTable.addMetric(['browser', this.t2 - this.t1])
        }

        return this.page
    }
}