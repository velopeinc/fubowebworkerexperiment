
import "regenerator-runtime/runtime";

self.addEventListener(
    "message",
    async function(e) {
      const urls = e.data;
      const images = await Promise.all(
        urls.map(async url => {
          try {
            const response = await fetch(url);
            const fileBlob = await response.blob();
            if (fileBlob.type === "image/jpeg")
              return URL.createObjectURL(fileBlob);
          } catch (e) {
              console.log("ERORR", e)
            return null;
          }
        })
      );
    
      self.postMessage(images);
    },
    false
  );