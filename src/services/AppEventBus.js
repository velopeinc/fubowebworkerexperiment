
/**
    App Event bus.
*/
class AppEventBus {
    /**
        Constructor
    */
    constructor() {
        this._bus = {};
    }

    /**
        Dispatch a message across a channel
        @param {string} channel - the channel name
        @param {string} message - the message to dispatch
    */
    dispatch(channel, message) {
        if (this._bus[channel] && this._bus[channel].constructor === Array) {
            const channels = [...this._bus[channel]];
            let i;
            let length = channels.length;
            let data = message;
            let obj;

            if ((data !== undefined && data !== null) && data.constructor !== Array) {
                data = [data];
            }

            for (i = 0; i < length; i++) {
                obj = channels[i];

                // if obj is undefined, it means the obj reference was removed
                // out of band, so this check will ensure the function exists before
                // executing the callback
                if (obj && typeof obj.cb === 'function') {
                    obj.cb.apply(obj.listener, data);
                }
            }
        }
    }

    /**
        Add a listener to the event bus
        @param {string} channel - the channel name
        @param {object} object - the object listener
        @param {function} callback - the object's callback method to apply
        @return {function} - unsubscribe function specific to the subscription being
        created
    */
    subscribe(channel, object, callback) {
        if (!this._bus[channel]) {
            this._bus[channel] = [];
        }

        this._bus[channel].push({
            listener: object,
            cb: callback
        });

        return this.unsubscribe.bind(this, channel, object);
    }

    /**
        Remove all specified listeners from the event bus
        @param {string} channel - the channel name
        @param {object} object - the object listener to remove
    */
    unsubscribe(channel, object) {
        if (this._bus[channel] && this._bus[channel].constructor === Array) {
            let i;

            for (i = 0; i < this._bus[channel].length; i++) {
                if (object === this._bus[channel][i].listener) {
                    this._bus[channel].splice(i, 1);
                    i--;
                }
            }
        }
    }

    /**
        Returns all listeners for a specified channel
        @param {string} channel - the channel name
        @return {object[]} array of listeners for the channel if exists, otherwise null
    */
    getSubscribers(channel) {
        if (channel && this._bus[channel]) {
            return this._bus[channel];
        } else {
            return null;
        }
    }
}

/** @type {EventBus} **/
export default new AppEventBus();