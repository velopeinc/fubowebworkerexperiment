// @flow
//http://statsapi.mlb.com/api/v1/schedule?hydrate=game(content(editorial(recap))),decisions&date=2018-06-10&sportId=1

import { createClient } from 'pexels'
import imageUrls from './data'

const apiKey1 = '563492ad6f917000010000014207ca9d10e847d0803f6d53536ed062'  //trixewow@gmail.com
const apiKey2 = '563492ad6f91700001000001a5c92363b92d4235b2b476d284b4cdb8'      //rachel@velope.tv

export default class DataService {

    static getImageUrls(query = `Nature`) {
        console.log("GET IMAGES", query)
        const client = createClient(apiKey2)

        return new Promise((resolve) => {
            let urls = [];
            return client.photos.search({ query, per_page: 80, size:'medium'})
                .then((data:any) => {
                    if(data.photos){
                        urls = data.photos.map((photo) => photo.src.medium)
                    } else {
                        console.log("DATA LIMIT", data)
                        debugger
                        urls = imageUrls
                    }
                    return resolve(urls)
             })
        })
       
    }
}



