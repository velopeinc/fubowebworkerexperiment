//@flow

const dataStore = new Map()
const listeners = {}

const notifyListeners = (key) => {
    if(!listeners[key]) return

    listeners[key].forEach((listener) => {
        listener([...dataStore[key]])
    })
}

export default class DataStore {

    static addDataListener(key: string, callback: Function): void{
        if(!listeners[key]) {
            listeners[key] = []
        }
        listeners[key].push(callback)
    }

    static removeDataListeners(key: string, callback: Function): void {
        const callbacks = listeners[key]
        if(callbacks){
            const filtered = callbacks.filter(callback => {
                return callback === callback
            })
            listeners[key] = filtered
        }
    }

    static storeData (key:string, data: any) {
        dataStore[key] = data
        notifyListeners(key)
    }
}